//Project created for demo of React Skills

1) use npm i to install dependencies. Create-react-app is used to make build.
2) localhost://3000 is used as default port
3) react 16.12 is used and this application is build with hooks
4) Components are divided into two major categories stateless and statefull
5) All stateless components are reusable and are under components folder
6) Statefull components are under container folder
7) This application is made responsive for mobile devices
8) Design principles such as KISS, YAGNI, Single responsibility are used
9) Naming convention are kept as much understandable as possible
10) Lifting state up concept is used
11) Unit test cases written partially in react testing library. Use npm test.
12) CSS libraries bootstrap is used.
13) Rick and morty standard api is implemented with features like 
    search by name, ascending, descending order, filter by gender, origin, species

