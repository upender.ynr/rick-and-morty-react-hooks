import React  from 'react';
import Landing from './containers/Landing';

function App() {
  
  return (
    <Landing />
  );
}

export default App;
