import React from "react";
import PropTypes from "prop-types";
import '../styles/components/select.css'

export default function Select(props) {

  return (
        <div className={'select-container ' + props.className}>
            <label htmlFor={props.label}>{props.label}</label>
           <select id={props.label} onClick={props.onClick}>
            {props.list.map(function optionsMaker(elem){
            return <option key={elem.value} value={elem.value}>{elem.label}</option>
            })}
            </select>
        </div>
        );
  }

  Select.propTypes = {
    list: PropTypes.array
  }