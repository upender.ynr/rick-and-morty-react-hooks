import React from "react";
import PropTypes from "prop-types";
import '../styles/components/button.css'

export default function Button(props) {

  return (  
        <button    
                data-testid={props.name}
                className= {'button ' + props.className}            
                disabled={props.disabled}
                type={props.type}
                onClick={props.handler}
                >
                {props.value || props.children}
               
        </button>       
        );
  }

  Button.propTypes = {
      disabled: PropTypes.bool,
      handler: PropTypes.func,
      name: PropTypes.string,
      type: PropTypes.string
  }