import React from "react";
import PropTypes from "prop-types";
import '../styles/components/userDetail.css'

export default function UserDetail(props) {
          
  return (
      <div className={'user-detail ' + props.className} id={props.id}>
    <figure className='img'><img alt={props.name} src={props.image}></img></figure>
    <h1 className='user__title'>{props.name}</h1>
     <h3 className='user__detail'>Status: {props.status}</h3>
     <h3 className='user__detail'>Species: {props.species}</h3>
     <h3 className='user__detail'>Type: {props.type}</h3>
     <h3 className='user__detail'>Gender: {props.gender}</h3>
     <h3 className='user__detail'>Origin: {props.origin.name}</h3>
     <h3 className='user__detail'>Location: {props.location.name}</h3>
     </div>
           
        );
  }

  UserDetail.propTypes = {
    status: PropTypes.string,
    name: PropTypes.string,
    gender: PropTypes.string,
    type: PropTypes.string,
    origin: PropTypes.object,
    location: PropTypes.object
  }