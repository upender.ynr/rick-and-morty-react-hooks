import React from "react";
import PropTypes from "prop-types";
import '../styles/components/filter.css'

export default function Filter(props) {

  return (
        <fieldset className={'filter-container ' + props.className}>
            <legend>{props.filterType}</legend>
            {props.checkboxList && props.checkboxList.map(function checkboxes(elem){
              return <div key={elem.id}>
              <input                
                className= 'checkbox'
                id={elem.id} 
                name={elem.name}
                onChange={elem.handler}
                type={elem.type}>
              </input>
            <label htmlFor= {elem.id}>{elem.id}</label>
            </div>
          })}
        </fieldset>
        );
  }

  Filter.propTypes = {
    list: PropTypes.array
  }