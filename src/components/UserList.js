import React from "react";
import UserDetail from "./UserDetail";
import PropTypes from "prop-types";
import '../styles/components/userList.css'

export default function UserList(props) {

  return (
      <div className={'user-list ' + props.className}>
        {props.list && props.list.map(function mapUserDetail(elem){
        return <UserDetail key={elem.id}
          name={elem.name}
          id={elem.id}
          image={elem.image}
          status={elem.status}
          species={elem.species}
          type={elem.type}
          gender={elem.gender}
          origin={elem.origin}
          location={elem.location}
          ></UserDetail>
        })} 
      </div> 
      );
  }

  UserList.propTypes = {
    list: PropTypes.any
  }