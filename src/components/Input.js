import React from "react";
import PropTypes from "prop-types";
import '../styles/components/input.css'

export default function Input(props) {

  return (
        <div className={'input-container ' + props.className}>
            <label htmlFor= {props.id}>{props.label}</label>
            <input
                    data-testid={props.id}
                    className='input'
                    id={props.id} 
                    name={props.name}
                    onChange={props.handler}
                    type={props.type}>
            </input>
            {props.isError && <label>{props.errorMessage}</label>}
        </div>
        );
  }

  
  Input.propTypes = {
    id: PropTypes.string,
    name: PropTypes.string,
    handler: PropTypes.func,
    type: PropTypes.string
  }