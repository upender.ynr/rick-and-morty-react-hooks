import axios from 'axios';
import { CHARACTER_LIST } from '../config/apiUrl.js'
 
export default function LandingService() {

   return axios.get(CHARACTER_LIST);

      
}

