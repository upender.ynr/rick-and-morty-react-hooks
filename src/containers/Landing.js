import React, { useState, useEffect, useReducer } from "react";
import Input from "../components/Input";
import Button from "../components/Button";
import LandingService from "../services/LandingService";
import UserList from "../components/UserList";
import Filter from '../components/Filter';
import { dynamicSort } from '../helpers/utils.js';
import Select from '../components/Select';
import PropTypes from "prop-types";
import { Dictionary } from '../config/dictionary';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/containers/landing.css';

export default function Landing(props) {

  const {gender , species, origin, status} = Dictionary.filter;
  const [search, setSearch] = useState("");
  const [ characterList, setCharacterList] = useState('');
  const [error, setError] = useState({isError:false, message:''});
  const [filterVal, setFilterVal] = useState([]);
  const [sortOrder, setSortOrder] = useState('');
  const [state, dispatch] = useReducer(reducer, 
    {'Male':false,'Female':false,
    'Human':false,'Alien':false,
    'Earth (Replacement Dimension)':false,'Earth (C-137)':false,'Abadango':false,
    'Alive':false,'Dead':false}
    );
  const isDisabled = validateSearch();

  useEffect(() => {
    LandingService().then((res)=>setCharacterList(res.data.results)).catch(function(err){setError({isError:true,message:'Loading character Failed'})});
  },[]);

  useEffect(() => {
    let filterValTemp;
    
    if(characterList){
      filterValTemp= characterList.filter(function filterVal(elem){
        return ((state.Female && elem.gender==="Female") 
        || (state.Male && elem.gender==="Male")
        || (state.Alive && elem.status==="Alive")
        || (state.Dead && elem.status==="Dead")
        || (state.Human && elem.species==="Human")
        || (state.Alien && elem.species==="Alien")
        || (state['Earth (C-137)'] && elem.origin.name==="Earth (C-137)")
        || (state['Earth (Replacement Dimension)'] && elem.origin.name==="Earth (Replacement Dimension)")
        || (state.Abadango && elem.origin.name==="Abadango")
        )
      })
    } else { 
     filterValTemp =[]
    }

    setFilterVal(filterValTemp)
  },[state,search,characterList])

  function validateSearch() {
     return search.length > 0;
  }

  function ascDscOrder(event){
    let value= event.target.value;
    let order= value ==='asc' ? '' : '-';
    if( value!==sortOrder) {
      setSortOrder(value);
      let filterValTemp= filterVal;
      let characterListTemp = characterList;
      if(event.target.value) {
        filterValTemp.sort(dynamicSort(order+"name"));
        characterListTemp.sort(dynamicSort(order+"name"));
      }
      setFilterVal(filterVal);
      setCharacterList(characterList);
    }
  }

  function reducer(state, action) {
    switch (action.type) {
      case gender.type.male:
        return Object.assign({}, state,{'Male': action.value});
      case gender.type.female:
        return Object.assign({}, state,{'Female': action.value});
        case species.type.human:
        return Object.assign({}, state,{'Human': action.value});
      case species.type.alien:
        return Object.assign({}, state,{'Alien': action.value});
      case origin.type.earthR:
        return Object.assign({}, state,{'Earth (Replacement Dimension)': action.value});
        case origin.type.earthC:
        return Object.assign({}, state,{'Earth (C-137)': action.value});
      case origin.type.abadango:
        return Object.assign({}, state,{'Abadango': action.value});
        case status.type.alive:
        return Object.assign({}, state,{'Alive': action.value});
      case status.type.dead:
        return Object.assign({}, state,{'Dead': action.value});
      default:
        throw new Error();
    }
  }

  function handleSubmit(event) {
   
    let filterValTemp; 
    if(characterList) {
      let filteredList=  filterVal.length>0 ? filterVal : characterList;
      let searchKey = new RegExp(search.toLowerCase(), 'g');
      filterValTemp = filteredList.filter(function(elem){
        return elem.name.toLowerCase().match(searchKey)
      })
    }
    setFilterVal(filterValTemp)
    event.preventDefault();
  }

  function handler(event){
    return dispatch({type: event.target.name, value:event.target.checked})
  }

  return (
      <form onSubmit={handleSubmit} className="landing">
        <label className='error'>{ error.isError && error.message}</label>
        <div className='search row'>
          <Input
              id='search'
              name='search'
              label={Dictionary.searchBox.label}
              handler={(e)=>{
                setSearch(e.target.value)}}
              type='text'
              isError={false}
          >
          </Input>
          <Button
              disabled={!isDisabled}
              name='searchBtn'
              type='submit'
          >
              {Dictionary.button.search.label}
          </Button>
          <div className='col-md-3 col-sm-2'>
            <Select  
                      onClick={ascDscOrder}
                      label={Dictionary.sort.label}
                      list={[{label:'<-select->',value:''},{label:Dictionary.sort.type.ascending,value:'asc'}, {label:Dictionary.sort.type.descending,value:'dsc'}]}>
            </Select>
          </div>
          
        </div>
        <div className='row'>
          <div className='col-md-3 col-sm-12'>
            <Filter
              filterType={species.label}
              checkboxList={[{id:'Human',name: species.type.human, type:'checkbox', handler:handler},
              {id:'Alien',name:species.type.alien, type:'checkbox' , handler:handler}]}>
            </Filter>
            <Filter 
              filterType={gender.label}
              checkboxList={[
                {id:'Male',name: gender.type.male, type:'checkbox', handler:handler},
                {id:'Female',name: gender.type.female, type:'checkbox', handler:handler}]}>
            </Filter>
            <Filter
              filterType={origin.label}
              checkboxList={[{id:'Earth (Replacement Dimension)',name:origin.type.earthR, type:'checkbox', handler:handler},
              {id:'Earth (C-137)',name: origin.type.earthC, type:'checkbox', handler:handler},
              {id:'Abadango',name: origin.type.abadango, type:'checkbox', handler:handler}]}>
            </Filter>
            <Filter
              filterType={status.label}
              checkboxList={[{id:'Alive',name: status.type.alive, type:'checkbox', handler:handler},
              {id:'Dead',name:status.type.dead, type:'checkbox', handler:handler}]}>
            </Filter>
          </div>
          <div className='col-md-9 col-sm-12'><UserList list={(filterVal.length >0) ? filterVal : characterList}></UserList></div>  
        </div>
        
      </form>
  );
}

Landing.propTypes = {
  Dictionary: PropTypes.object,
  characterList: PropTypes.object,
  search: PropTypes.string,
  error: PropTypes.object,
  validateSearch: PropTypes.func,
  ascDscOrder: PropTypes.func,
  handler: PropTypes.func,
  handleSubmit: PropTypes.func,
  reducer: PropTypes.func,
  sortOrder: PropTypes.string,
  state: PropTypes.object,
  filterVal: PropTypes.array,
};