import React from "react";
import { render, cleanup, fireEvent } from "@testing-library/react";
import Landing from "./Landing";
import '@testing-library/jest-dom'

describe("Landing", () => {
      afterEach(cleanup)    
       it("should render", () => { 
            const landing = render(<Landing />);
            expect(landing).toBeTruthy();  
    });
});

describe("Landing search box", () => {
    it('should change state on value change and enable the button', () => {
        const { getByTestId } = render(<Landing />);        
        let searchInput= getByTestId("search");  
        const searchBtn= getByTestId("searchBtn");
        expect(searchBtn).toBeDisabled();    
        fireEvent.change(searchInput, { target: { value: 'rick' } } );
        expect(searchBtn).not.toBeDisabled();
    });
});
