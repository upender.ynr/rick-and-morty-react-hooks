export const Dictionary = {
    filter : {
        gender: {
            label: 'Filter Gender',
            type: {
                male: 'Male',
                female: 'Female'
            }
        },
        origin: {
            label: 'Filer Origin',
            type: {
                earthR: 'Earth (Replacement Dimension)',
                earthC: 'Earth (C-137)',
                abadango: 'Abadango'
            }
        },
        status: {
            label: "Filter Status",
            type: {
                alive: 'Alive',
                dead: 'Dead'
            }
        },
        species: {
            label: "Filter Species",
            type: {
                human: 'Human',
                alien: 'Alien'
            }
        }
    },
    sort: {
        label: 'Sort By Name',
        type: {
            ascending: 'Ascending',
            descending: 'Descending'
        }
    },
    button: {
        search: {
            label: 'Search'
        }
    },
    searchBox: {
        label: 'Search Character'
    }
}